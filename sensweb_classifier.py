#!/usr/bin/env python3
from bs4 import BeautifulSoup
import io
import joblib
import re
import requests
import sys
from unidecode import unidecode
sys.setrecursionlimit(100000)


# the vectorizer
PATH_TO_VECTORIZER = "./vectorizer_balanced_20k_features.pkl"
# the classifier
PATH_TO_CLASSIFIER = "./classifier_balanced_20k_features.pkl"


def extract_text(URL):
    # Fetch the URL
    data = requests.get(URL)
    html = data.text
    # Convert data into a BeautifulSoup object
    soup = BeautifulSoup(html, features="lxml")
    # Extract text
    clean_content = None
    if soup is not None:
        # Rip out scripts
        [script.extract() for script in soup(["script", "style"])]
        # Extract the text
        text = [line.strip() for line
                in unidecode(soup.get_text()).split('\n')]
        text = " ".join([' '.join(line.split())
                         for line in text if line != ''])
        text = text.replace('\n', ' ').replace('\r', '')
        content_txt = []
        if len(text) > 0:
            obj_content = text.replace('\n', ' ').replace('\r', '')
            content_txt.append(obj_content)
        content_text = " ".join(content_txt).strip()
        clean_content = re.sub(r'http\S+', '',
                               content_text, flags=re.MULTILINE)
    return clean_content


def classify_content(TEXT):
    text_tf = vectorizer.transform([TEXT])[0]
    # Predictions
    predicted = classifier.predict_proba(text_tf).tolist()[0]
    # Return the results with the probabilities
    category = labels[predicted.index(max(predicted))]
    return {"pr_Clear": predicted[0],
            "pr_Ethnicity": predicted[1],
            "pr_Health": predicted[2],
            "pr_Politics": predicted[3],
            "pr_Religion": predicted[4],
            "pr_Sexual": predicted[5],
            "category": category}


def main():
    global vectorizer
    global classifier
    global labels

    url = "https://www.cdc.gov/coronavirus/2019-ncov/symptoms-testing/symptoms.html"

    # 1) Load the vectorizer and the classifier
    vectorizer = joblib.load(io.BytesIO(
        open(PATH_TO_VECTORIZER, 'rb').read()))
    classifier = joblib.load(io.BytesIO(
        open(PATH_TO_CLASSIFIER, 'rb').read()))
    labels = classifier.classes_.tolist()

    # 2) Fetch the HTML and extract text
    html_text_content = extract_text(URL=url)

    # 3) Classify the content
    classifier_output = classify_content(html_text_content)

    # 3) Print the results
    print("URL: %s" % (url))
    print("category: %s" % (classifier_output["category"]))
    print("probability_Clear: %s" % (classifier_output["pr_Clear"]))
    print("probability_Ethnicity: %s" % (classifier_output["pr_Ethnicity"]))
    print("probability_Health: %s" % (classifier_output["pr_Health"]))
    print("probability_Politics: %s" % (classifier_output["pr_Politics"]))
    print("probability_Religion: %s" % (classifier_output["pr_Religion"]))
    print("probability_Sexual: %s" % (classifier_output["pr_Sexual"]))


if __name__ == "__main__":
    main()
