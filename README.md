# README #
This is the official repository for the classifier for "sensitive content" presented in "[Identifying Sensitive URLs at Web-Scale](https://doi.org/10.1145/3419394.3423653)" (IMC'20).

The repository contains:

1. the code and the modules needed to run the "balanced classifier with 20,000 features" presented in the paper;

2. the list of Curlie categories that were used to train the classifier.


## Dependencies ##
* *Script*: **sensweb_classifier.py**

    * *External dependencies*: **bs4 joblib sklearn unidecode**

### Installing Dependencies on Debian GNU/Linux bullseye/sid + Python 3.8.5 ####
To run our framework, you will need python3 with some additional libraries. Python3 and all the external libraries can be installed using the command line:
```
$ sudo apt-get install python3 python3-pip
$ pip3 install --upgrade bs4 joblib sklearn unidecode
```


## Repository Content ##
1. *Package:* **vectorizer_balanced_20k_features.pkl**: contains the component responsible for converting a text into a list of tokens.
2. *Package:* **classifier_balanced_20k_features.pkl**: contains the component for classifying the list of tokens produced in output by "vectorizer_balanced_20k_features.pkl".
3. *Script:* **sensweb_classifier.py**: fetches the content of a URL and uses the classifier to associate a probability score to the content that is inspected.
4. *Directory:* **senscat_to_curliecat**: contains the list of [Curlie.org](https://curlie.org/en) that were used to define the 5 sensitive categories (i.e., *Ethnicity*, *Health*, *Political Beliefs*, *Religion*, *Sexual Orientation*) to which a web page belongs to. 

    For each one of the 5 sensitive categories we include two files which contain:
	
    - a list of Curlie-categories that were used to define the sensitive-category (e.g., *cat_Health.txt*) and
   
    - a list of Curlie-categories that we consider as _not_ relevant for that specific sensitive category (e.g., *cat_not_Health.txt*).
    
    A sixth *non-sensitive* category is derived by combining together all the Curlie-categories that do not belong to anyone of the 5 sensitive categories.

### Running the Code ###
1. Clone the repository.
2. Run "python3 sensweb_classifier.py".


## Additional Information ##
The design and the evaluation of the classifier are available in our 
[IMC 2020 paper](https://doi.org/10.1145/3419394.3423653):
> Srdjan Matic, Costas Iordanou, Georgios Smaragdakis and Nikolaos Laoutaris
In Proceedings of the 2020 ACM Internet Measurement Conference
(IMC '20), October 27 - 29, 2020, Pittsburgh, PA, USA

If you use the classifier, please cite our work:
```
@inproceedings{IMC2020-GDPR,
  author = {Srdjan Matic and Costas Iordanou and Georgios Smaragdakis and Nikolaos Laoutaris},
  title =  {{Identifying Sensitive URLs at Web-Scale}},
  booktitle = {Proceedings of ACM IMC 2020},
  month = {October},
  year = {2020},
  address ={Pittsburgh, PA, USA}
}
```


### Contact ###
info: s.matic@inet.tu-berlin.de
